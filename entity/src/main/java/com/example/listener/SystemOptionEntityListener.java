package com.example.listener;

import com.example.system.entity.SystemOptionEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import javax.persistence.PostPersist;
import javax.persistence.PostRemove;
import javax.persistence.PostUpdate;

@Component
@Slf4j
public class SystemOptionEntityListener {

    private final AuditLogService auditLogService;

    public static final String AUDIT_ACTIVITY = "system_option";

    @Autowired
    public SystemOptionEntityListener(AuditLogService auditLogService) {
        this.auditLogService = auditLogService;
    }

    @PostPersist
    public void methodInvokedAfterPersist(SystemOptionEntity entity) {
        log.debug("Created " + entity);
        auditLogService.createLogs(AUDIT_ACTIVITY, "Created " + entity);
    }

    @PostUpdate
    public void methodInvokedAfterUpdate(SystemOptionEntity entity) {
        log.debug("Updated " + entity);
        auditLogService.createLogs(AUDIT_ACTIVITY, "Updated " + entity);
    }

    @PostRemove
    public void methodInvokedAfterDelete(SystemOptionEntity entity) {
        log.debug("Deleted " + entity);
        auditLogService.createLogs(AUDIT_ACTIVITY, "Deleted " + entity);
    }

}

