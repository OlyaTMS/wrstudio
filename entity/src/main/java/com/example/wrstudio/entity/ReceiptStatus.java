package com.example.wrstudio.entity;

public enum ReceiptStatus {

    PENDING, PAYED, CANCELLED
}
