package com.example.wrstudio.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "CLIENT")
public class Client extends Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CLIENT_ID")
    private Long id;

    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.DETACH})
    @JoinTable(name = "CLIENT_FILIAL",
            joinColumns = @JoinColumn(name = "CLIENT_ID"),
            inverseJoinColumns = @JoinColumn(name = "FILIAL_ID")
    )
    private List<Filial> filials;

    @OneToMany(mappedBy = "client")
    private List<Visit> visits;

    @ManyToMany(mappedBy = "clients", cascade = {CascadeType.REFRESH, CascadeType.DETACH})
    private List<Master> masters;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "CLIENT_COURSE",
            joinColumns = @JoinColumn(name = "CLIENT_ID"),
            inverseJoinColumns = @JoinColumn(name = "COURSE_ID")
    )
    private List<Course> courses;

    @Embedded
    private ContactDetails contactDetails;
}

