package com.example.wrstudio.entity;

import lombok.Getter;
import com.example.wrstudio.annotation.View;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Getter
@Entity
@View
@Table(name = "CLIENT_RECEIPT")
public class ClientReceipt {

    @Id
    @Column(name = "ID")
    private Long id;

    @Column(name = "RECEIPT_ID")
    private Long receiptId;

    @Column(name = "TOTAL_AMOUNT")
    private BigDecimal totalAmount;

    @Column(name = "PAYER")
    private String payer;

    @Column(name = "PAYER_PHONE")
    private String payerPhone;
}
