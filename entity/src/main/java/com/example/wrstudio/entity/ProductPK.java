package com.example.wrstudio.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Embeddable
public class ProductPK implements Serializable {

    @Column(name = "CODE")
    private String code;

    @Column(name = "CODE_PART")
    private String codePart;
}
