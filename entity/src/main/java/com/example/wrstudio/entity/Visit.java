package com.example.wrstudio.entity;

import lombok.*;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "VISIT")
public class Visit {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "VISIT_TIME")
    private OffsetDateTime time;

    @OneToOne(cascade = CascadeType.ALL,orphanRemoval = true)
    @JoinColumn(name = "VISIT_NOTES_ID")
    private Notes notes;

    @ManyToOne
    @JoinColumn(name = "CLIENT_ID")
    private Client client;

    @ManyToMany(mappedBy = "visits", cascade = {CascadeType.PERSIST, CascadeType.DETACH})
    private List<Master> masters;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "RECEIPT_ID")
    private Receipt receipt;

}
