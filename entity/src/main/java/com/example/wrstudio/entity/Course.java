package com.example.wrstudio.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "COURSE")
public class Course{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "PRICE")
    private BigDecimal price;

    @OneToMany(mappedBy = "course",cascade = {CascadeType.REFRESH,CascadeType.DETACH})
    private List<Master> masters;

    @ManyToMany(mappedBy = "courses",cascade = {CascadeType.REFRESH,CascadeType.DETACH})
    private List<Client> clients;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "RECEIPT_ID")
    private Receipt receipt;
}
