package com.example.wrstudio.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "RECEIPT")
public class Receipt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "AMOUNT")
    private BigDecimal amount;

    @OneToMany(mappedBy = "receipt",cascade = CascadeType.PERSIST)
    private List<Procedure> procedures;

    @OneToMany(mappedBy = "receipt",cascade = CascadeType.PERSIST)
    private List<Product> products;

    @OneToMany(mappedBy = "receipt",cascade = CascadeType.PERSIST)
    private List<Course> courses;

    @OneToOne(mappedBy = "receipt")
    private Visit visit;

    @Builder.Default
    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private ReceiptStatus status = ReceiptStatus.PENDING;
}
