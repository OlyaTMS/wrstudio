package com.example.wrstudio.entity;

import com.example.wrstudio.converter.MasterTypeConverter;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@Entity
@Table(name = "PERSON")
public class Master extends Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Convert(converter = MasterTypeConverter.class)
    private MasterType masterType;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.DETACH})
    @JoinColumn(name = "FILIAL_ID")
    private Filial filial;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.DETACH})
    @JoinTable(name = "MASTER_CLIENT",
            joinColumns = @JoinColumn(name = "MASTER_ID"),
            inverseJoinColumns = @JoinColumn(name = "CLIENT_ID")
    )
    private List<Client> clients;

    @ManyToMany( cascade = {CascadeType.PERSIST, CascadeType.DETACH})
    @JoinTable(name = "MASTER_VISIT",
            joinColumns = @JoinColumn(name = "MASTER_ID"),
            inverseJoinColumns = @JoinColumn(name = "VISIT_ID")
    )
    private List<Visit> visits;

    @ManyToOne(cascade = {CascadeType.REFRESH,CascadeType.DETACH})
    @JoinColumn(name = "COURSE_ID")
    private Course course;

    @Embedded
    private ContactDetails contactDetails;
}
