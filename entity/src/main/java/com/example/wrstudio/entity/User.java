package com.example.wrstudio.entity;

import com.example.wrstudio.converter.RoleConverter;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "USER")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "LOGIN")
    private String login;

    @Column(name = "PASSWORD")
    private String password;

    @Convert(converter = RoleConverter.class)
    private Role role;

    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.DETACH}, fetch = FetchType.LAZY)
    @JoinTable(name = "USER_PERM",
            joinColumns = @JoinColumn(name = "USER_ID"),
            inverseJoinColumns = @JoinColumn(name = "PERM_ID")
    )
    private Set<Permission> permissions;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "USER_FILIAL",
            joinColumns = @JoinColumn(name = "USER_ID"),
            inverseJoinColumns = @JoinColumn(name = "FILIAL_ID")
    )
    private Set<Filial> filials;

    @Embedded
    private ContactDetails contactDetails;

}
