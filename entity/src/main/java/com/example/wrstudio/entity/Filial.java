package com.example.wrstudio.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "FILIAL")
public class Filial {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "FILIAL_NAME")
    private String filialName;

    @OneToMany(mappedBy = "filial")
    private List<Master> masters;

    @ManyToMany(mappedBy = "filials", cascade = {CascadeType.PERSIST, CascadeType.DETACH})
    private List<Client> clients;

    @Embedded
    private ContactDetails contactDetails;
}
