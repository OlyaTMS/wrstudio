package com.example.wrstudio.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
@RequiredArgsConstructor
public enum MasterType {
    HAIRDRESSER(Values.HAIRDRESSER),
    NAILMASTER(Values.NAILMASTER),
    COSMETOLOGIST(Values.COSMETOLOGIST),
    LASHMAKER(Values.LASHMAKER),
    BROWMASTER(Values.BROWMASTER);

    private static final Map<String, MasterType> MAP = Arrays.stream(MasterType.values())
            .collect(Collectors.toMap(MasterType::getValue, Function.identity()));

    private final String value;

    public static MasterType getByValue(String value) {
        if (Objects.isNull(value)) {
            return null;
        }

        return MAP.get(value);
    }

    public static class Values{
        public static final String HAIRDRESSER = "Hairdresser";
        public static final String NAILMASTER = "NailMaster";
        public static final String COSMETOLOGIST = "Cosmetologist";
        public static final String LASHMAKER = "Lashmaker";
        public static final String BROWMASTER = "Browmaster";
    }
}
