package com.example.wrstudio.entity;

import lombok.*;

import javax.persistence.*;
import java.time.OffsetDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "NOTES")
public class Notes {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "TIME")
    private OffsetDateTime time;

    @Column(name = "DESCRIPTION")
    private String description;

    @OneToOne(mappedBy = "notes")
    private Visit visit;

}
