package com.example.wrstudio.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@PrimaryKeyJoinColumn(name = "CLIENT_ID")
@Entity
@Table(name = "VIP_CLIENT")
public class VipClient extends Client {

    @Column(name = "VIP_CARD")
    private String vipCard;

    @Column(name = "DISCOUNT")
    private Long discount;

    @Column(name = "LOYALTY_PROGRAM")
    private String loyaltyProgram;

    @Column(name = "LOYALTY_AMOUNT")
    private Long loyaltyAmount;
}
