package com.example.wrstudio.converter;


import com.example.wrstudio.entity.MasterType;

import javax.persistence.AttributeConverter;

public class MasterTypeConverter implements AttributeConverter<MasterType,String> {

    @Override
    public String convertToDatabaseColumn(MasterType attribute) {
        return attribute.getValue();
    }

    @Override
    public MasterType convertToEntityAttribute(String dbData) {
        return MasterType.getByValue(dbData);
    }

}
