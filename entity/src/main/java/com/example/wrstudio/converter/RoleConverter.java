package com.example.wrstudio.converter;

import com.example.wrstudio.entity.Role;

import javax.persistence.AttributeConverter;

public class RoleConverter implements AttributeConverter<Role,String> {

    @Override
    public String convertToDatabaseColumn(Role attribute) {
        return attribute.getValue();
    }

    @Override
    public Role convertToEntityAttribute(String dbData) {
        return Role.getByValue(dbData);
    }

}
