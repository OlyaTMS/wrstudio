package com.example.system.entity;

import com.example.listener.SystemOptionEntityListener;
import lombok.*;

import javax.persistence.*;

@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EntityListeners(SystemOptionEntityListener.class)
@Entity
@Table(name = "SYSTEM_OPTION")
public class SystemOptionEntity {

    @Id
    @Column(name = "ID")
    private String id;

    @Column(name = "VALUE")
    private String value;
}

