package com.example.wrstudio.mapper;

import com.example.wrstudio.dto.ClientDto;
import com.example.wrstudio.entity.Client;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@Mapper
public interface ClientMapper {

    Client map(ClientDto dto);
    ClientDto mapToDto(Client entity);
}
