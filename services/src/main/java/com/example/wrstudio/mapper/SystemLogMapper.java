package com.example.wrstudio.mapper;

import com.example.system.entity.SystemLogEntity;
import com.example.wrstudio.dto.SystemLogDto;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface SystemLogMapper {

    SystemLogDto mapToDto(SystemLogEntity entity);

    List<SystemLogDto> mapListToDto(List<SystemLogEntity> entities);
}