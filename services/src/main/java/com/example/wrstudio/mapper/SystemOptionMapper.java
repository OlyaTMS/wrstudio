package com.example.wrstudio.mapper;

import com.example.system.entity.SystemOptionEntity;
import com.example.wrstudio.dto.SystemOptionDto;
import org.mapstruct.Mapper;

@Mapper
public interface SystemOptionMapper {

    SystemOptionDto mapToDto(SystemOptionEntity entity);
}
