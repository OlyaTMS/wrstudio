package com.example.wrstudio.services;

import com.example.wrstudio.dto.SystemLogDto;

import java.util.List;

public interface SystemLogService {

    List<SystemLogDto> getAll();

    void createLogs(String activity, String message);
}
