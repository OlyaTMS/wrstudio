package com.example.wrstudio.services.map;

import com.example.wrstudio.entity.Client;
import com.example.wrstudio.services.ClientService;
import com.example.wrstudio.services.config.MapImplementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class ClientMapService extends AbstractMapService<Client, Long> implements ClientService {

    private static final Map<Long, Client> resource = new HashMap<>();

    @Override
    public Map<Long, Client> getResource() {
        return resource;
    }

    @Override
    public Collection<Client> findByName(String name) {
        return null;
    }
}

