package com.example.wrstudio.services;

import com.example.wrstudio.dto.SystemOptionDto;

import java.util.List;

public interface SystemOptionService {

    List<SystemOptionDto> getAll();

    void save(SystemOptionDto option);

    SystemOptionDto getById(String optionKey);
}
