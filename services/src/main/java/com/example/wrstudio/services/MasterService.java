package com.example.wrstudio.services;

import com.example.wrstudio.entity.Master;

import java.util.Collection;

public interface MasterService extends CrudService<Master,Long> {

    Collection<Master> findByServiceType(String serviceType);
}
