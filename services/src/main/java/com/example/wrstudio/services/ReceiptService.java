package com.example.wrstudio.services;

public interface ReceiptService {

    void cancelReceipt(Long id);

    void markReceiptAsPayed(Long id);
}
