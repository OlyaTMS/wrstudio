package com.example.wrstudio.services.jpa;


import com.example.wrstudio.entity.BaseEntity;
import com.example.wrstudio.services.BaseJpaService;
import com.example.wrstudio.services.CrudService;

import java.util.Collection;
import java.util.NoSuchElementException;

public abstract class AbstractJpaService<T extends BaseEntity<ID>, ID> implements CrudService<T, ID>, BaseJpaService<T, ID> {

    @Override
    public T findById(ID id) {
        return getRepository().findById(id)
                .orElseThrow(() -> new NoSuchElementException("not found"));
    }

    @Override
    public void save(T entity) {
        getRepository().save(entity);
    }

    @Override
    public Collection<T> findAll() {
        return getRepository().findAll();
    }

    @Override
    public void delete(ID id) {
        getRepository().deleteById(id);
    }
}
