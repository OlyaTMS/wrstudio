package com.example.wrstudio.services.jpa;

import com.example.wrstudio.entity.Master;
import com.example.wrstudio.services.MasterService;
import com.example.wrstudio.services.config.JpaImplementation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;

@JpaImplementation
public class MasterJpaService extends AbstractJpaService<Master,Long> implements MasterService {
    @Override
    public JpaRepository<Master, Long> getRepository() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<Master> findByServiceType(String serviceType) {
        throw new UnsupportedOperationException();
    }
}
