package com.example.wrstudio.services;

import com.example.wrstudio.dto.ClientDto;
import com.example.wrstudio.entity.Client;
import com.example.wrstudio.specification.filter.ClientFilter;
import org.springframework.data.domain.Page;

import java.util.Collection;
import java.util.List;

public interface ClientService extends CrudService<Client,Long> {

    Collection<Client> findByName(String name);

    default List<ClientDto> findClient(ClientDto clientDto) {
        throw new UnsupportedOperationException();
    }

    default Page<ClientDto> search(ClientFilter clientFilter) {
        throw new UnsupportedOperationException();
    }
}
