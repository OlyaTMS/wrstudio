package com.example.wrstudio.services.jpa;

import com.example.wrstudio.dto.ClientDto;
import com.example.wrstudio.entity.Client;
import com.example.wrstudio.mapper.ClientMapper;
import com.example.wrstudio.repository.ClientRepository;
import com.example.wrstudio.services.ClientService;
import com.example.wrstudio.services.config.JpaImplementation;
import com.example.wrstudio.specification.ClientSpecification;
import com.example.wrstudio.specification.SearchableRepository;
import com.example.wrstudio.specification.SearchableService;
import com.example.wrstudio.specification.filter.ClientFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@JpaImplementation
public class ClientJpaService extends AbstractJpaService<Client,Long> implements ClientService, SearchableService<Client> {

    private final ClientRepository clientRepository;
    private final ClientMapper mapper;

    @Override
    public List<ClientDto> findClient(ClientDto clientDto) {
        ExampleMatcher caseInsensitiveExMatcher = ExampleMatcher.matchingAll().withIgnoreCase();
        Example<Client> example = Example.of(mapper.map(clientDto), caseInsensitiveExMatcher);
        return clientRepository.findAll(example).stream().map(mapper::mapToDto).collect(Collectors.toList());
    }

    @Override
    public JpaRepository<Client, Long> getRepository() {
        throw new UnsupportedOperationException();
    }

    @Override
    public SearchableRepository<Client, ?> getSearchRepository() {
        return clientRepository;
    }

    @Override
    public Collection<Client> findByName(String name) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Page<ClientDto> search(ClientFilter clientFilter) {
        return searchPage(clientFilter).map(mapper::mapToDto);
    }
}
