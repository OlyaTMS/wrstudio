package com.example.wrstudio.services.map;


import com.example.wrstudio.entity.Master;
import com.example.wrstudio.services.MasterService;
import com.example.wrstudio.services.config.MapImplementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class MasterMapService extends AbstractMapService<Master, Long> implements MasterService {

    private static final Map<Long, Master> resource = new HashMap<>();

    @Override
    public Map<Long, Master> getResource() {
        return resource;
    }

    @Override
    public Collection<Master> findByServiceType(String serviceType) {
        return null;
    }
}
