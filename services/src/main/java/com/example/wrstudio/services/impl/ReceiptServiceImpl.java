package com.example.wrstudio.services.impl;

import com.example.wrstudio.aspect.ActivityLog;
import com.example.wrstudio.entity.ReceiptStatus;
import com.example.wrstudio.repository.ReceiptRepository;
import com.example.wrstudio.services.ReceiptService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class ReceiptServiceImpl implements ReceiptService {

    private final ReceiptRepository receiptRepository;

    @ActivityLog(activity = "receipt_cancel", value = "Receipt cancelled {id}")
    @Override
    public void cancelReceipt(Long id) {
        receiptRepository.findById(id).orElseThrow().setStatus(ReceiptStatus.CANCELLED);
    }

    @ActivityLog(activity = "receipt_payed", value = "Receipt payed {id}")
    @Override
    public void markReceiptAsPayed(Long id) {
        receiptRepository.findById(id).orElseThrow().setStatus(ReceiptStatus.PAYED);
    }
}

