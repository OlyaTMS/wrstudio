package com.example.wrstudio.services.impl;

import com.example.system.entity.SystemOptionEntity;
import com.example.system.repository.SystemRepository;
import com.example.wrstudio.dto.SystemOptionDto;
import com.example.wrstudio.mapper.SystemOptionMapper;
import com.example.wrstudio.services.SystemLogService;
import com.example.wrstudio.services.SystemOptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class SystemOptionServiceImpl implements SystemOptionService {

    private final SystemRepository systemRepository;
    private final SystemLogService systemLogService;
    private final SystemOptionMapper mapper;

    @Override
    public List<SystemOptionDto> getAll() {
        return systemRepository.findAll().stream().map(mapper::mapToDto).collect(Collectors.toList());
    }

    @Transactional(transactionManager = "systemTransactionManager")
    @Override
    public void save(SystemOptionDto option) {
        SystemOptionEntity entity = systemRepository.findById(option.getId())
                .orElse(SystemOptionEntity.builder().id(option.getId()).build());

        entity.setValue(option.getValue());
        systemRepository.save(entity);
    }

    @Override
    public SystemOptionDto getById(String optionKey) {
        return systemRepository.findById(optionKey).map(mapper::mapToDto).orElseThrow();
    }
}
