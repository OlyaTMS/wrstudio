package com.example.wrstudio.specification.filter;


import com.example.wrstudio.entity.Client;
import com.example.wrstudio.entity.Client_;
import com.example.wrstudio.specification.ClientSpecification;
import lombok.Builder;
import lombok.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;

import java.util.Objects;

import static com.example.wrstudio.specification.filter.Filter.DEFAULT_PAGE_SIZE;

@Builder
@Value
public class ClientFilter implements Filter<Client> {

    private static final Sort DEFAULT_SORTING = Sort.by(Sort.Direction.DESC, Client_.CREATED_AT);

    String term;
    Integer pageNumber;
    String firstName;
    String lastName;
    boolean isActive;

    @Override
    public Pageable getPageable() {
        int page = Objects.isNull(pageNumber) ? 0 : pageNumber - 1;
        return PageRequest.of(page, DEFAULT_PAGE_SIZE, DEFAULT_SORTING);
    }

    @Override
    public Specification<Client> getSpecification() {
        return ClientSpecification.builder().filter(this).build();
    }
}
