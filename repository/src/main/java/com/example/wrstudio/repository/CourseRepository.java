package com.example.wrstudio.repository;


import com.example.wrstudio.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepository extends JpaRepository<Course,Long> {
}
