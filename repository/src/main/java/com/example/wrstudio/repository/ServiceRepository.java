package com.example.wrstudio.repository;

import com.example.wrstudio.entity.Procedure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServiceRepository extends JpaRepository<Procedure,Long> {
}
