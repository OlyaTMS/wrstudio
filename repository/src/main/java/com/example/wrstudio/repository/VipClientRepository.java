package com.example.wrstudio.repository;

import com.example.wrstudio.entity.VipClient;

public interface VipClientRepository extends PersonRepository<VipClient,Long> {
}
