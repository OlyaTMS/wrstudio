package com.example.wrstudio.repository;

import com.example.wrstudio.entity.ClientReceipt;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClientReceiptRepository extends JpaRepository<ClientReceipt,Long> {

    List<ClientReceipt> findAll();
}
