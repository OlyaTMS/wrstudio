package com.example.wrstudio.repository;

import com.example.wrstudio.entity.Filial;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilialRepository extends JpaRepository<Filial,Long> {
}
