package com.example.wrstudio.repository;

import com.example.wrstudio.entity.Product;
import com.example.wrstudio.entity.ProductPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, ProductPK> {
}
