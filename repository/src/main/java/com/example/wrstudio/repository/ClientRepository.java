package com.example.wrstudio.repository;

import com.example.wrstudio.entity.Client;
import com.example.wrstudio.specification.SearchableRepository;

public interface ClientRepository extends PersonRepository<Client,Long>, SearchableRepository<Client, Long> {
}
