package com.example.wrstudio.repository;


import com.example.wrstudio.entity.Master;

public interface MasterRepository extends PersonRepository<Master,Long> {
}
