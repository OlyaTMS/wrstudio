package com.example.wrstudio.cache;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Setter
@Getter
@Component
@ConfigurationProperties(prefix = "cache.ttl")
public class CacheProperties {

    private int clients;
    private int masters;
}

