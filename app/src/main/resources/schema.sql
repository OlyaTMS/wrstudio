create table CLIENT
(
    CLIENT_ID BIGINT auto_increment
        primary key,
    CREATED_AT TIMESTAMP not null,
    CREATED_BY VARCHAR(255),
    LAST_MODIFIED_BY VARCHAR(255),
    LAST_MODIFIED_AT TIMESTAMP not null,
    FIRST_NAME VARCHAR(255),
    FULL_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255),
    BUILDING VARCHAR(255),
    CITY VARCHAR(255),
    PHONE VARCHAR(255),
    STREET VARCHAR(255)
);

create table FILIAL
(
    ID BIGINT auto_increment
        primary key,
    BUILDING VARCHAR(255),
    CITY VARCHAR(255),
    PHONE VARCHAR(255),
    STREET VARCHAR(255)
);

create table CLIENT_FILIALS
(
    CLIENTS_CLIENT_ID BIGINT not null,
    FILIALS_ID BIGINT not null,
    constraint FKBA655MD211KEC545GH69AJWN8
        foreign key (FILIALS_ID) references FILIAL (ID),
    constraint FKK36WC87QSUSA8V2HIHOT6F25F
        foreign key (CLIENTS_CLIENT_ID) references CLIENT (CLIENT_ID)
);

create table NOTES
(
    ID BIGINT auto_increment
        primary key,
    TIME TIMESTAMP,
    VISIT_ID BIGINT
);

create table PERMISSION
(
    ID BIGINT auto_increment
        primary key,
    NAME VARCHAR(255)
);

create table RECEIPT
(
    ID BIGINT auto_increment
        primary key,
    AMOUNT DECIMAL(19,2)
);

create table COURSE
(
    ID BIGINT auto_increment
        primary key,
    DESCRIPTION VARCHAR(255),
    PRICE DECIMAL(19,2),
    RECEIPT_ID BIGINT,
    constraint FK9FSL3B24DHIDGGRU24CRKFQYI
        foreign key (RECEIPT_ID) references RECEIPT (ID)
);

create table CLIENT_COURSES
(
    CLIENTS_CLIENT_ID BIGINT not null,
    COURSES_ID BIGINT not null,
    constraint FK85HK7S3SUBXBSONVWOI96CR9Y
        foreign key (CLIENTS_CLIENT_ID) references CLIENT (CLIENT_ID),
    constraint FKMNKIG01IUWL8P5E1W1Q7LR28U
        foreign key (COURSES_ID) references COURSE (ID)
);

create table MASTER
(
    ID BIGINT auto_increment
        primary key,
    CREATED_AT TIMESTAMP not null,
    CREATED_BY VARCHAR(255),
    LAST_MODIFIED_BY VARCHAR(255),
    LAST_MODIFIED_AT TIMESTAMP not null,
    FIRST_NAME VARCHAR(255),
    FULL_NAME VARCHAR(255),
    LAST_NAME VARCHAR(255),
    BUILDING VARCHAR(255),
    CITY VARCHAR(255),
    PHONE VARCHAR(255),
    STREET VARCHAR(255),
    MASTER_TYPE VARCHAR(255),
    COURSE_ID BIGINT,
    FILIAL_ID BIGINT,
    constraint FK8TUEEYR66ASWEIEYI6NP2N0GY
        foreign key (FILIAL_ID) references FILIAL (ID),
    constraint FKTE0KSGKRAY3ICKN3P27596ECA
        foreign key (COURSE_ID) references COURSE (ID)
);

create table MASTER_CLIENTS
(
    MASTERS_ID BIGINT not null,
    CLIENTS_CLIENT_ID BIGINT not null,
    constraint FK95OWQA8H3R53HU06EQ7KX7CBP
        foreign key (MASTERS_ID) references MASTER (ID),
    constraint FKPE8KO1NY8VTCAO3NBU9IMJJWH
        foreign key (CLIENTS_CLIENT_ID) references CLIENT (CLIENT_ID)
);

create table PROCEDURE
(
    ID BIGINT auto_increment
        primary key,
    PRICE DECIMAL(19,2),
    SERVICE_NAME VARCHAR(255),
    RECEIPT_ID BIGINT,
    constraint FKMNIBAV5LDN0TDJ4MQX8VL1USH
        foreign key (RECEIPT_ID) references RECEIPT (ID)
);

create table PRODUCT
(
    CODE VARCHAR(255) not null,
    CODE_PART VARCHAR(255) not null,
    NAME VARCHAR(255),
    PRICE DECIMAL(19,2),
    RECEIPT_ID BIGINT,
    primary key (CODE, CODE_PART),
    constraint FKEOAXT4SLQ51CIS1E0V436GH2P
        foreign key (RECEIPT_ID) references RECEIPT (ID)
);

create table USER
(
    ID BIGINT auto_increment
        primary key,
    BUILDING VARCHAR(255),
    CITY VARCHAR(255),
    PHONE VARCHAR(255),
    STREET VARCHAR(255),
    LOGIN VARCHAR(255),
    PASSWORD VARCHAR(255),
    ROLE VARCHAR(255)
);

create table USER_FILIALS
(
    USER_ID BIGINT not null,
    FILIALS_ID BIGINT not null,
    primary key (USER_ID, FILIALS_ID),
    constraint FK88NLYUFJ3X4GP44VTF5QOSN1N
        foreign key (USER_ID) references USER (ID),
    constraint FKN6XTE0KCL15MDW4N5AWGYQ5S
        foreign key (FILIALS_ID) references FILIAL (ID)
);

create table USER_PERMISSIONS
(
    USERS_ID BIGINT not null,
    PERMISSIONS_ID BIGINT not null,
    primary key (USERS_ID, PERMISSIONS_ID),
    constraint FK52H90R477V9SJJHQ9R8SRI5WH
        foreign key (USERS_ID) references USER (ID),
    constraint FKF22AKIK4MGTJXKF2ISH2RTF42
        foreign key (PERMISSIONS_ID) references PERMISSION (ID)
);

create table VIP_CLIENT
(
    DISCOUNT BIGINT,
    LOYALTY_AMOUNT BIGINT,
    LOYALTY_PROGRAM VARCHAR(255),
    VIP_CARD VARCHAR(255),
    CLIENT_ID BIGINT not null
        primary key,
    constraint FKEBMH5DWSB5VJSWUS3TJEWK9VA
        foreign key (CLIENT_ID) references CLIENT (CLIENT_ID)
);

create table VISIT
(
    ID BIGINT auto_increment
        primary key,
    TIME TIMESTAMP,
    CLIENT_CLIENT_ID BIGINT,
    NOTES_ID BIGINT,
    RECEIPT_ID BIGINT,
    constraint FKCLHB27KF34VVI0X5CUXO9L67N
        foreign key (CLIENT_CLIENT_ID) references CLIENT (CLIENT_ID),
    constraint FKDTWDS0QQ9ID8T981CPM6UV3EU
        foreign key (NOTES_ID) references NOTES (ID),
    constraint FKDV6XG17D6E58QM307XWHDYGB
        foreign key (RECEIPT_ID) references RECEIPT (ID)
);

create table MASTER_VISITS
(
    MASTERS_ID BIGINT not null,
    VISITS_ID BIGINT not null,
    constraint FK7AVVNL35UCS095IB522S4R5HT
        foreign key (VISITS_ID) references VISIT (ID),
    constraint FKAUYOR8T9BBDRSB637B5GLKBFP
        foreign key (MASTERS_ID) references MASTER (ID)
);

alter table NOTES
    add constraint FKH0VY66GO2V11XKB99J79JWN6G
        foreign key (VISIT_ID) references VISIT (ID);