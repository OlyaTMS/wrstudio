package com.example.wrstudio.config;


import com.example.wrstudio.entity.*;
import com.example.wrstudio.repository.ClientRepository;
import com.example.wrstudio.repository.ReceiptRepository;
import com.example.wrstudio.repository.VisitRepository;
import lombok.RequiredArgsConstructor;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.List;

@RequiredArgsConstructor
@Component("mainRunner")
public class Runner implements CommandLineRunner {

    @Value("${spring.datasource.password:undefined}")
    private String dbPwd;

    @Qualifier("customEncryptor")
    @Autowired
    StringEncryptor stringEncryptor;

    private final ClientRepository clientRepository;
    private final VisitRepository visitRepository;
    private final ReceiptRepository receiptRepository;


    @Override
    public void run(String... args) throws Exception {

//        createClient();
//        createVisit();
//        createReceipt();

       // encryption();

    }
    private void createClient() {
        Client client1 = Client.builder()
                .firstName("Olya")
                .lastName("Rusanovich")
                .contactDetails(ContactDetails.builder()
                        .city("Zaslavl")
                        .building("98")
                        .street("Sovetslaya")
                        .phone("+375-44-123-45-67")
                        .build())
                .build();
        Client client2 = Client.builder()
                .firstName("Yulia")
                .lastName("Luschic")
                .contactDetails(ContactDetails.builder()
                        .city("Zaslavl")
                        .building("63")
                        .street("Sovetskaya")
                        .phone("+375-44-982-32-15")
                        .build())
                .build();
        Client client3 = Client.builder()
                .firstName("Ulyana")
                .lastName("Mihalchenko")
                .contactDetails(ContactDetails.builder()
                        .city("Minsk")
                        .building("31")
                        .street("Kolesnikova")
                        .phone("+375-44-124-61-83")
                        .build())
                .build();
        clientRepository.saveAll(Arrays.asList(client1,client2,client3));
    }

    private void createVisit() {
        List<Client> clients = clientRepository.findAll();

        Visit visit1 = Visit.builder().time(OffsetDateTime.now())
                .client(clients.get(0))
                .notes(Notes.builder()
                        .description("peeling")
                        .build())
                .build();
        Visit visit2 = Visit.builder().time(OffsetDateTime.now())
                .client(clients.get(1))
                .notes(Notes.builder()
                        .description("manicure")
                        .build())
                .build();
        clients.get(1).setVisits(Arrays.asList(visit2));
        Visit visit3 = Visit.builder().time(OffsetDateTime.now())
                .client(clients.get(2))
                .notes(Notes.builder()
                        .description("lashExtension")
                        .build())
                .build();
        visitRepository.saveAll(Arrays.asList(visit1,visit2,visit3));
    }

    private void createReceipt() {
        List<Visit> visits = visitRepository.findAll();

        Receipt receipt1 = Receipt.builder()
                .amount(BigDecimal.TEN)
                .visit(visits.get(0))
                .build();
        Procedure procedure1 = Procedure.builder()
                .procedureName("peeling")
                .price(BigDecimal.ONE)
                .receipt(receipt1)
                .build();
        Procedure procedure2 = Procedure.builder()
                .procedureName("manicure")
                .price(BigDecimal.ONE)
                .receipt(receipt1)
                .build();
        Procedure procedure3 = Procedure.builder()
                .procedureName("lashExtension")
                .price(BigDecimal.ONE)
                .receipt(receipt1)
                .build();
        receipt1.setProcedures(Arrays.asList(procedure1,procedure2));
        visits.get(0).setReceipt(receipt1);
        receiptRepository.save(receipt1);

        Receipt receipt2 = Receipt.builder()
                .amount(BigDecimal.TEN)
                .visit(visits.get(1))
                .build();
        visits.get(1).setReceipt(receipt2);
        receiptRepository.save(receipt2);
        Receipt receipt3 = Receipt.builder()
                .amount(BigDecimal.TEN)
                .visit(visits.get(2))
                .build();
        visits.get(2).setReceipt(receipt3);
        receiptRepository.save(receipt3);
        visitRepository.saveAll(visits);

    }


        private void encryption() {
//            String pwd = "dev_pswd";
//            String encrypt = stringEncryptor.encrypt(pwd);
//            System.out.println(encrypt);
//            String decrypt = stringEncryptor.decrypt(encrypt);
//            System.out.println(decrypt);
            System.out.println(dbPwd);
        }
}
