package com.example.wrstudio.api;

import com.example.wrstudio.services.ReceiptService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RequestMapping("/receitps")
@RestController
public class ReceiptController {

    private final ReceiptService receiptService;

    @PutMapping("/{id}/cancel")
    public void cancelReceipt(@PathVariable Long id) {
        receiptService.cancelReceipt(id);
    }

    @PutMapping("/{id}/payed")
    public void markReceiptAsPayed(@PathVariable Long id) {
        receiptService.markReceiptAsPayed(id);
    }
}

