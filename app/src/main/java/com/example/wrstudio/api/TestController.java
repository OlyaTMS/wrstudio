package com.example.wrstudio.api;

import com.example.wrstudio.entity.ClientReceipt;
import com.example.wrstudio.repository.ClientReceiptRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/")
@RestController
public class TestController {

    private final ClientReceiptRepository clientReceiptRepository;

    @GetMapping("/view")
    public List<ClientReceipt> getView() {
        return clientReceiptRepository.findAll();
    }
}
