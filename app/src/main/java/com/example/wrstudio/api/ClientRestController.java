package com.example.wrstudio.api;

import com.example.wrstudio.dto.ClientDto;
import com.example.wrstudio.entity.Client;
import com.example.wrstudio.services.ClientService;
import com.example.wrstudio.specification.filter.ClientFilter;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/clients")
@RestController
public class ClientRestController {

    private final ClientService clientService;

    @GetMapping
    public Page<ClientDto> getAll() {
        return clientService.search(null);
    }

    @ApiOperation(value = "Search clients", response = Client.class, responseContainer = "Page")
    @GetMapping("/search")
    public Page<ClientDto> search(@RequestParam(required = false) String term,
                                 @RequestParam(required = false) Integer pageNumber) {

        ClientFilter clientFilter = ClientFilter.builder()
                .pageNumber(pageNumber)
                .term(term)
                .build();
        return clientService.search(clientFilter);
    }

    @ApiOperation(value = "Search clients", response = Client.class, responseContainer = "Page")
    @GetMapping("/search/by_name")
    public Page<ClientDto> searchByName(@RequestParam(required = false) String name) {
        return clientService.search(null);
    }

    @ApiOperation(value = "Find clients", response = Client.class, responseContainer = "List")
    @PutMapping("/find")
    public List<ClientDto> findClients(@ApiParam(value = "Search example") @RequestBody ClientDto clientDto) {
        return clientService.findClient(clientDto);
    }

    @GetMapping("/{id}")
    public ClientDto getById(@PathVariable("id") Long clientId) {
        return null;
    }

    @PostMapping
    public void create(@RequestBody ClientDto clientDto) {
    }

    @PutMapping("/{id}")
    public void update(@PathVariable("id") Long clientId, @RequestBody ClientDto clientDto) {
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long clientId) {
        clientService.delete(clientId);
    }
}
