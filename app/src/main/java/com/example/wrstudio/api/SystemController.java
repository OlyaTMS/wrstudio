package com.example.wrstudio.api;

import com.example.wrstudio.dto.SystemLogDto;
import com.example.wrstudio.dto.SystemOptionDto;
import com.example.wrstudio.services.SystemLogService;
import com.example.wrstudio.services.SystemOptionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/system")
@RestController
public class SystemController {

    private final SystemLogService systemLogService;
    private final SystemOptionService systemOptionService;

    @GetMapping("/logs")
    public List<SystemLogDto> getLogs() {
        return systemLogService.getAll();
    }

    @GetMapping("/options")
    public List<SystemOptionDto> getSystem() {
        return systemOptionService.getAll();
    }

    @GetMapping("/options/{id}")
    public SystemOptionDto getById(@PathVariable("id") String optionKey) {
        return systemOptionService.getById(optionKey);
    }

    @PutMapping("/options")
    public void saveOption(@RequestBody SystemOptionDto option) {
        systemOptionService.save(option);
    }
}
