package com.example.wrstudio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan({"com.example.listener", "com.example.wrstudio"})
@SpringBootApplication
public class WrStudioApplication {

    public static void main(String[] args) {
        SpringApplication.run(WrStudioApplication.class, args);
    }

}
