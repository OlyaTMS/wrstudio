package com.example.wrstudio.controller;

import com.example.wrstudio.services.ClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

@RequiredArgsConstructor
@Controller
public class ClientController {

    private final ClientService clientService;
}
