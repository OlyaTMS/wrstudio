package com.example.wrstudio.controller;

import com.example.wrstudio.services.MasterService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

@RequiredArgsConstructor
@Controller
public class MasterController {

    private final MasterService masterService;
}
